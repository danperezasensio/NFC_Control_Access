#!/usr/local/bin/python3

import threading
import queue
import sys

import sqlite3

import reader
import db


def main():
    """
    Reads a tag and opens a door if the UID received exists in a database.
    """
    dbo = db.Database("/var/www/data/database.db")
    print(dbo)
    try:
        dbo.con()
    except sqlite3.Error as e:
        print(e)
        sys.exit(0)
    else:
        # Creates a Reader object
        rdr = reader.Reader()
        rdr.init()

        # Creates an event in order to blink the RGB LED.
        evt = threading.Event()
        evt.clear()

        # Creates a queue in order to receive the LED modes.
        q = queue.Queue()

        # Creates a thread in order to obtain asynchronous behaviour.
        t = threading.Thread(target=reader.led, args=(q, evt,))
        evt.set()
        t.start()

        # READ mode
        q.put(0)

        run = True
        while run:
            try:
                uid = rdr.read()
                if uid is not None:
                    try:
                        name = dbo.chck(uid)  # Checks UID into the database.
                    except ValueError:
                        q.put(2)  # BAN mode.
                        rdr.ban()
                        q.put(0)
                    else:
                        q.put(1)  # OPEN mode.
                        rdr.open()
                        try:
                            dbo.add_r(name, "Principal")  # Appends UID to the register.
                        except sqlite3.Error:
                            run = False
                        else:
                            q.put(0)  # READ mode.
            except KeyboardInterrupt:
                run = False

        # Termination of different tasks properly.
        try:
            dbo.dis()
        except sqlite3.Error:
            print("ERROR -> Could not close the database properly.")
        finally:
            evt.clear()
            t.join()
            rdr.init()


if __name__ == '__main__':
    main()
